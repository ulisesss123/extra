public class XO {
  public static boolean getXO (String str) {
		short x = 0;
		short o = 0;
		boolean notContainsXorO = true;
		String str2 = str.toLowerCase();
		for(int i=0; i<str2.length(); i++) {
      if(str2.charAt(i) =='x'){
				x++;
			}
			else if(str2.charAt(i) == 'o') {
				o++;
			}
			else {
				notContainsXorO = true;
			}
		}
		return ((x==o) && notContainsXorO);
  }
}