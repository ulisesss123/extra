import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Actividad2 {
  
public static String test ="\"Luke Skywalker\" \"172\" \"male\" \"human\" \"Jedi Order, Rebel Alliance\";\n\"R2-D2\" \"96\" \"n/a\" \"droid\" \"Rebel Alliance\";\n" +
    "\"C-3PO\" \"167\" \"n/a\" \"droid\" \"Rebel Alliance\";\n\"Darth Vader\" \"202\" \"male\" \"human\" \"Galactic Empire, Sith\";\n" +
    "\"Leia Organa\" \"150\" \"female\" \"human\" \"Rebel Alliance, The Resistance\";\n\"Owen Lars\" \"178\" \"male\" \"human\" \"Tatooine, Lars Farm\";\n" +
    "\"Beru Whitesun lars\" \"165\" \"female\" \"human\" \"Tatooine, Lars Farm\";\n\"R5-D4\" \"97\" \"n/a\" \"droid\" \"Tatooine\";\n" +
    "\"Biggs Darklighter\" \"183\" \"male\" \"human\" \"Rebel Alliance\";\n\"Obi-Wan Kenobi\" \"182\" \"male\" \"human\" \"Jedi Order\";\n" +
    "\"Yoda\" \"66\" \"male\" \"unknown\" \"Jedi Order\";\n\"Jek Tono Porkins\" \"180\" \"male\" \"human\" \"Rebel Alliance\";\n" +
    "\"Jabba Desilijic Tiure\" \"175\" \"hermaphrodite\" \"hutt\" \"Jabba the Hutt's Palace\";\n\"Han Solo\" \"180\" \"male\" \"human\" \"Rebel Alliance\";\n" +
    "\"Chewbacca\" \"228\" \"male\" \"Wookiee\" \"Rebel Alliance, The Resistance\";\n\"Anakin Skywalker\" \"188\" \"male\" \"human\" \"Jedi Order\";";
    
    public static void busca_no_hombres(){
        String expR = ".*(\"f|dr|her).*";
        final Pattern pattern = Pattern.compile(expR, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(test);                                                                               
        while (matcher.find()) {
            System.out.println(matcher.group(0));
        }
    }
    public static void busca_no_rebeldes(){
        String expR = ".*(\"Jedi Order\"|\"Ja|\"T|\"G).*";
        final Pattern pattern = Pattern.compile(expR, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(test);                                                                               
        while (matcher.find()) {
            System.out.println(matcher.group(0));
        }
    }
    public static void busca_menos_metro(){
        String expR = ".*(\"\\d{1,2}\").*";
        final Pattern pattern = Pattern.compile(expR, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(test);                                                                               
        while (matcher.find()) {
            System.out.println(matcher.group(0));
        }
    }
    public static void main(String[] args) {
        System.out.println("Listar todos los personajes que no sean hombres:\n");  busca_no_hombres();
        System.out.println("\nListar todos los personajes que no son Rebeldes:\n");  busca_no_rebeldes();
        System.out.println("\nListar todos los personajes que midan menos del metro:\n");  busca_menos_metro();
    }
}